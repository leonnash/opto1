package com.chmu.opto1.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.chmu.opto1.Calc;
import com.chmu.opto1.Graph;
import com.chmu.opto1.MyReader;
import com.chmu.opto1.model.Intersection;
import com.chmu.opto1.model.Ray;

import java.util.List;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = Graph.WIDTH;
        config.height = Graph.HEIGHT;
        Calc.init(2);

        System.out.println("Плоскость? y/n");
        if (!MyReader.readString().equalsIgnoreCase("y")) {
            System.out.println("Сфера или Элипс? 1/2");
            List<Object> objects;
            if (MyReader.readString().equalsIgnoreCase("1")) {
                objects = Calc.getAllRaysOnSphereUI();
                Graph.r = (Double) objects.get(1);
            } else {
                objects = Calc.getAllRaysOnEllipsoidUI();
                Graph.abc = (double[]) objects.get(1);
            }
            Graph.center = (double[]) objects.get(0);
            Graph.intersections = (List<Intersection>) objects.get(2);
            Graph.rays = (List<Ray>) objects.get(3);
        } else {
            Graph.rays = Calc.getAllRaysOnPlaneUI();
        }
        new LwjglApplication(new Graph(), config);
    }
}
