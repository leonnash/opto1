package com.chmu.opto1.model;

public class Intersection {

    private double[] point;
    private double t;

    public Intersection(double[] point, double t) {
        this.point = point;
        this.t = t;
    }

    public double[] getPoint() {
        return point;
    }

    public void setPoint(double[] point) {
        this.point = point;
    }

    public double getT() {
        return t;
    }

    public void setT(double t) {
        this.t = t;
    }
}
