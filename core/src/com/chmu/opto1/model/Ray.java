package com.chmu.opto1.model;

import com.badlogic.gdx.graphics.Color;
import com.chmu.opto1.Calc;

public class Ray {

    private double[] point;
    private double[] e;
    private Color color;
    private double t = 200;


    public Ray(double[] point, double[] e) {
        this.point = point;
        this.e = e;
        this.color = Color.BLACK;
    }

    public Ray(double[] point, double[] e, Color color) {
        this.point = point;
        this.e = e;
        this.color = color;
    }

    public double[] getPoint() {
        return point;
    }

    public void setPoint(double[] point) {
        this.point = point;
    }

    public double[] getE() {
        return e;
    }

    public void setE(double[] e) {
        this.e = e;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String toString(boolean reflection) {
        return String.format("Характеристики " + (reflection ? "отражённого" : "преломлённого") + " луча:\n" +
                "Начало луча:%s\n" +
                "Вектор направления:%s\n", Calc.getString(point), Calc.getString(e));
    }

    public double getT() {
        return t;
    }

    public void setT(double t) {
        this.t = t;
    }
}
