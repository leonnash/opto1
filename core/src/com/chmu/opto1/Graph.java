package com.chmu.opto1;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.chmu.opto1.model.Intersection;
import com.chmu.opto1.model.Ray;

import java.util.List;

public class Graph extends ApplicationAdapter {

    public final static int WIDTH = 1000;
    public final static int HEIGHT = 1000;
    public static double[] center;
    public static double[] abc;
    public static double r;
    public static List<Ray> rays;
    public static List<Intersection> intersections;
    private ShapeRenderer renderer;

    @Override
    public void create() {
        renderer = new ShapeRenderer();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(255, 255, 255, 255);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.setColor(Color.BLACK);
        if (center != null) {
            if (abc != null) {
                renderer.ellipse(WIDTH / 2 + (float) center[0] - (float) abc[0], HEIGHT / 2 + (float) center[1] - (float) abc[1], (float) abc[0]*2, (float) abc[1]*2);
            }
            renderer.circle(WIDTH / 2 + (float) center[0], HEIGHT / 2 + (float) center[1], (float) r);
        }
        for (Ray ray1 : rays) {
            renderer.setColor(ray1.getColor());
            float[] ray = Calc.createRay2D(ray1, ray1.getT());
            renderer.circle(WIDTH / 2 + ray[0], HEIGHT / 2 + ray[1], 3);
            renderer.line(WIDTH / 2 + ray[0], HEIGHT / 2 + ray[1], WIDTH / 2 + ray[2], HEIGHT / 2 + ray[3]);
        }
        renderer.end();
    }

    @Override
    public void dispose() {
        renderer.dispose();
    }
}
