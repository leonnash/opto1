package com.chmu.opto1;

import com.badlogic.gdx.graphics.Color;
import com.chmu.opto1.model.Intersection;
import com.chmu.opto1.model.Ray;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Calc {
    public static Scanner sc = new Scanner(System.in);
    private static int DIM = 3;
    private static double n1;
    private static double n2;


    public static void main(String[] args) {
		/*System.out.println("Введите размерность: ");
		DIM = sc.nextInt();
        */
        //intersectionWithPlaneUI();
        //intersectionWithSphereUI();
//        intersectionWithEllipsoidUI();
//        reflectedRayFromPlaneUI();
        //getAllRaysOnSphereUI();

    }

    private static void intersectionWithEllipsoidUI() {
        System.out.println("Введите радиус-вектор центра элипсоида: ");
        double[] p0 = readDoubleArray(DIM);
        System.out.println("Введите параметры элипсоида a b c: ");
        double[] abc = readDoubleArray(DIM);
        System.out.println("Введите радиус-вектор начала луча: ");
        double[] r0 = readDoubleArray(DIM);
        System.out.println("Введите вектор направления луча: ");
        double[] e = normalize(readDoubleArray(DIM));

        intersectionWithEllipsoid(p0, abc, r0, e);
    }

    private static List<Intersection> intersectionWithEllipsoid(double[] p0, double[] abc, double[] r0, double[] e) {
        List<Intersection> intersections = new ArrayList<Intersection>(2);

        double[] r0p0ABC = multABC(abc, sub(r0, p0));
        double[] eABC = multABC(abc, e);
        double znam = scalMult(eABC, eABC);

        double sqrt;
        if (abc.length == 3) {
            sqrt = Math.pow(scalMult(r0p0ABC, eABC), 2) - znam * (scalMult(r0p0ABC, r0p0ABC) - Math.pow(abc[0] * abc[1] * abc[2], 2));
        } else {
            sqrt = Math.pow(scalMult(r0p0ABC, eABC), 2) - znam * (scalMult(r0p0ABC, r0p0ABC) - Math.pow(abc[0] * abc[1], 2));
        }
        if (sqrt >= 0) {
            sqrt = Math.sqrt(sqrt);
            double t1 = (-scalMult(r0p0ABC, eABC) - sqrt) / znam;
            double t2 = (-scalMult(r0p0ABC, eABC) + sqrt) / znam;
            double[] intersection1 = sum(r0, mult(e, t1));
            double[] intersection2 = sum(r0, mult(e, t2));
            if (t1 == t2) {
                if (t2 < 0) {
                    System.out.println("Луч не пересекает элипсоид");
                } else {
                    System.out.println("Луч пересекает элипсоид в одной точке: " + getString(intersection2) + ", при t = " + t2);
                    intersections.add(new Intersection(intersection2, t2));
                }
            } else {
                if (t2 < 0) {
                    System.out.println("Луч не пересекает элипсоид");
                } else if (t1 < 0) {
                    System.out.println("Луч пересекает элипсоид в одной точке: " + getString(intersection2) + ", при t = " + t2);
                    intersections.add(new Intersection(intersection2, t2));
                } else {
                    System.out.println("Луч пересекает элипсоид в точках: " + getString(intersection1) + ", при t = " + t1);
                    System.out.println("И " + getString(intersection2) + ", при t = " + t2);
                    intersections.add(new Intersection(intersection1, t1));
                    intersections.add(new Intersection(intersection2, t2));
                }
            }
        } else {
            System.out.println("Луч не пересекает элипсоид");
        }
        return intersections;
    }

    private static double[] multABC(double[] abc, double[] vector) {
        double[] res = new double[vector.length];
        if (abc.length == 3) {
            res[0] = vector[0] * abc[1] * abc[2];
            res[1] = vector[1] * abc[0] * abc[2];
            res[2] = vector[2] * abc[0] * abc[1];
        } else {
            res[0] = vector[0] * abc[1];
            res[1] = vector[1] * abc[0];
        }
        return res;
    }

    public static void intersectionWithSphereUI() {
        System.out.println("Введите радиус-вектор центра сферы: ");
        double[] p0 = readDoubleArray(DIM);
        System.out.println("Введите радиус сферы: ");
        double R = MyReader.readDouble();
        ;
        System.out.println("Введите радиус-вектор начала луча: ");
        double[] r0 = readDoubleArray(DIM);
        System.out.println("Введите вектор направления луча: ");
        double[] e = normalize(readDoubleArray(DIM));

        intersectionWithSphere(p0, R, r0, e);
    }


    private static List<Intersection> intersectionWithSphere(double[] p0, double r, double[] r0, double[] e) {
        List<Intersection> intersections = new ArrayList<Intersection>(2);

        double[] r0p0 = sub(r0, p0);
        double sqrt = Math.pow(scalMult(r0p0, e), 2) - scalMult(r0p0, r0p0) + Math.pow(r, 2);

        if (sqrt >= 0) {
            sqrt = Math.sqrt(sqrt);
            double t1 = -scalMult(r0p0, e) - sqrt;
            double t2 = -scalMult(r0p0, e) + sqrt;
            double[] intersection1 = sum(r0, mult(e, t1));
            double[] intersection2 = sum(r0, mult(e, t2));
            if (t1 == t2) {
                if (t2 < 0) {
                    System.out.println("Луч не пересекает сферу");
                    return null;
                } else {
                    System.out.println("Луч пересекает сферу в одной точке: " + getString(intersection2) + ", при t = " + t2);
                    intersections.add(new Intersection(intersection2, t2));
                }
            } else {
                if (t2 < 0) {
                    System.out.println("Луч не пересекает сферу");
                    return null;
                } else if (t1 < 0) {
                    System.out.println("Луч пересекает сферу в одной точке: " + getString(intersection2) + ", при t = " + t2);
                    intersections.add(new Intersection(intersection2, t2));
                } else {
                    System.out.println("Луч пересекает сферу в точках: " + getString(intersection1) + ", при t = " + t1);
                    System.out.println("И " + getString(intersection2) + ", при t = " + t2);
                    intersections.add(new Intersection(intersection1, t1));
                    intersections.add(new Intersection(intersection2, t2));
                }
            }
        } else {
            System.out.println("Луч не пересекает сферу");
            return null;
        }
        return intersections;
    }

    public static List<Ray> getAllRaysOnPlaneUI() {
        System.out.println("Введите нормаль: ");
        double[] n = normalize(readDoubleArray(DIM));
        System.out.println("Введите радиус-вектор плоскости: ");
        double[] r0 = readDoubleArray(DIM);
        System.out.println("Введите радиус-вектор луча: ");
        double[] p0 = readDoubleArray(DIM);
        System.out.println("Введите вектор направления луча: ");
        double[] e = normalize(readDoubleArray(DIM));

        Intersection intersection = intersectionWithPlane(n, r0, p0, e);
        List<Ray> rays = new ArrayList<Ray>();
        Ray originalRay = new Ray(p0, e);
        //originalRay.setT(intersection.getT());
        rays.add(originalRay);
        boolean isConcave = scalMult(e, n) < 0;
        Ray reflectedRay = reflectedRay(n, e, intersection, isConcave);
        rays.add(reflectedRay);
        Ray refractedRay = refractedRay(n, e, intersection, isConcave);
        rays.add(refractedRay);
        //rays.add(new Ray(intersection.getPoint(), p0));
        rays.add(new Ray(intersection.getPoint(), n, Color.BROWN));
        rays.add(new Ray(intersection.getPoint(), mult(n, -1), Color.BROWN));
        Ray ray = new Ray(intersection.getPoint(), new double[]{n[1], n[0]}, Color.BLACK);
        ray.setT(5000);
        rays.add(ray);
        ray = new Ray(intersection.getPoint(), mult(ray.getE(), -1), Color.BLACK);
        ray.setT(5000);
        rays.add(ray);
        //rays.add(new Ray(intersection.getPoint(), p0));

        return rays;
    }

    private static Intersection intersectionWithPlane(double[] n, double[] r0, double[] p0, double[] e) {


        double znam = scalMult(n, e);
        double chisl = scalMult(n, sub(r0, p0));
        if (znam == 0) {
            if (chisl == 0) {
                System.out.println("Луч лежит на плоскости");
            } else {
                System.out.println("Луч параллелен плоскости");
            }
            return null;
        }

        double t = chisl / znam;

        if (t < 0) {
            System.out.println("Луч и плоскость не пересекаются");
            return null;
        } else if (t == 0) {
            System.out.println("Луч выходит из плоскости, точка пересения: " + getString(p0) + ", t = 0");
            return new Intersection(p0, 0);
        } else {
            double[] intersection = sum(p0, mult(e, t));
            System.out.println("Луч пересекает плоскость в точке: " + getString(intersection) + ", t = " + t);
            return new Intersection(intersection, t);
        }
    }

    private static double[] normalize(double[] vector) {
        double sqrt = norm(vector);
        for (int i = 0; i < vector.length; i++) {
            vector[i] /= sqrt;
        }
        return vector;
    }

    private static double norm(double[] vector) {
        return Math.sqrt(scalMult(vector, vector));
    }

    private static double[] mult(double[] e, double t) {
        double[] res = new double[e.length];
        for (int i = 0; i < res.length; i++) {
            res[i] = e[i] * t;
        }
        return res;
    }

    private static double[] div(double[] vector, double a) {
        double[] res = new double[vector.length];
        for (int i = 0; i < res.length; i++) {
            res[i] = vector[i] / a;
        }
        return res;
    }

    public static String getString(double[] vector) {
        StringBuilder res = new StringBuilder("(");
        for (int i = 0; i < vector.length; i++) {
            res.append(vector[i]).append((i == vector.length - 1) ? "" : "; ");
        }
        return res + ")";
    }

    private static boolean equals(double[] first, double[] second) {
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]) return false;
        }
        return true;
    }

    private static double[] sub(double[] first, double[] second) {
        double[] res = new double[first.length];
        for (int i = 0; i < first.length; i++) {
            res[i] = first[i] - second[i];
        }
        return res;
    }

    private static double[] sum(double[] first, double[] second) {
        double[] res = new double[first.length];
        for (int i = 0; i < first.length; i++) {
            res[i] = first[i] + second[i];
        }
        return res;
    }

    private static double[] readDoubleArray(int n) {
        double[] res = new double[n];
        for (int i = 0; i < n; i++) {
            res[i] = MyReader.readDouble();
        }
        return res;
    }

    private static double scalMult(double[] first, double[] second) {
        double res = 0;
        for (int i = 0; i < first.length; i++) {
            res += first[i] * second[i];
        }
        return res;
    }

    private static Ray reflectedRayFromPlaneUI() {
        System.out.println("Введите нормаль: ");
        double[] n = normalize(readDoubleArray(DIM));
        System.out.println("Введите радиус-вектор плоскости: ");
        double[] r0 = readDoubleArray(DIM);
        System.out.println("Введите радиус-вектор луча: ");
        double[] p0 = readDoubleArray(DIM);
        System.out.println("Введите вектор направления луча: ");
        double[] e = normalize(readDoubleArray(DIM));

        Intersection intersection = intersectionWithPlane(n, r0, p0, e);
        return reflectedRay(n, e, intersection, false);
    }

    private static Ray reflectedRay(double[] n, double[] e, Intersection intersection, boolean isConcave) {
        double coef = 1;
        if (isConcave) {
            coef = -1;
        }

        if (intersection != null) {
            double[] newE = sub(e, mult(n, 2 * coef * Math.abs(scalMult(e, n))));
            Ray ray = new Ray(intersection.getPoint(), normalize(newE), Color.BLUE);
            System.out.println(ray.toString(true));
            return ray;
        }
        return null;
    }

    private static Ray refractedRay(double[] n, double[] e, Intersection intersection, boolean isConcave) {
        double coef = 1;
        double n1 = Calc.n2;
        double n2 = Calc.n1;
        if (isConcave) {
//            coef = -1;
            n1 = Calc.n1;
            n2 = Calc.n2;
        }

        if (intersection != null) {
            double en = coef * Math.abs(scalMult(e, n));
            double[] newE = mult(sub(e, mult(n, en * (1.0 - Math.sqrt(1.0 + (n2 * n2 - n1 * n1) / (en * en * n1 * n1))))), n1 / n2);
            Ray ray = new Ray(intersection.getPoint(), normalize(newE), Color.GREEN);
            System.out.println(ray.toString(false));
            return ray;
        }
        return null;
    }


    public static List<Object> getAllRaysOnSphereUI() {
        List<Object> ans = new ArrayList<Object>();

        List<Ray> rays = new ArrayList<Ray>();
        List<Intersection> intersectionsToRender = new ArrayList<Intersection>();

        System.out.println("Введите радиус-вектор центра сферы: ");
        double[] p0 = readDoubleArray(DIM);
        System.out.println("Введите радиус сферы: ");
        double R = MyReader.readDouble();
        System.out.println("Введите радиус-вектор начала луча: ");
        double[] r0 = readDoubleArray(DIM);
        System.out.println("Введите вектор направления луча: ");
        double[] e = normalize(readDoubleArray(DIM));
        List<Intersection> intersections = intersectionWithSphere(p0, R, r0, e);
        Ray start = new Ray(r0, e);
        if (intersections != null && intersections.size() > 0) start.setT(intersections.get(0).getT());
        rays.add(start);
        if (intersections != null && intersections.size() > 0) {
            if (intersections.size() == 1) { // изнутри
                intersectionsToRender.add(intersections.get(0));
                double[] normVector = calculateNormOnSphere(intersections.get(0).getPoint(), p0);
                rays.add(reflectedRay(normVector,
                        e, intersections.get(0), false));
                rays.add(refractedRay(normVector,
                        e, intersections.get(0), false));
                rays.add(new Ray(intersections.get(0).getPoint(), normVector, Color.BROWN));
                rays.add(new Ray(intersections.get(0).getPoint(), mult(normVector, -1), Color.BROWN));
            } else { //снаружи
                intersectionsToRender.add(intersections.get(0));
                double[] normVector = calculateNormOnSphere(intersections.get(0).getPoint(), p0);
                rays.add(reflectedRay(normVector,
                        e, intersections.get(0), true));
                Ray refractedRay = refractedRay(normVector,
                        e, intersections.get(0), true);
                rays.add(refractedRay);
                rays.add(new Ray(intersections.get(0).getPoint(), normVector, Color.BROWN));
                rays.add(new Ray(intersections.get(0).getPoint(), mult(normVector, -1), Color.BROWN));

                List<Intersection> fromInside = intersectionWithSphere(p0, R, refractedRay.getPoint(), refractedRay.getE());
                refractedRay.setT(fromInside.get(1).getT());
                intersectionsToRender.add(fromInside.get(1));
                normVector = calculateNormOnSphere(fromInside.get(1).getPoint(), p0);
                rays.add(reflectedRay(normVector,
                        refractedRay.getE(), fromInside.get(1), false));
                rays.add(refractedRay(normVector,
                        refractedRay.getE(), fromInside.get(1), false));

                rays.add(new Ray(fromInside.get(1).getPoint(), normVector, Color.BROWN));
                rays.add(new Ray(fromInside.get(1).getPoint(), mult(normVector, -1), Color.BROWN));

            }
        }
        ans.add(p0);
        ans.add(R);
        ans.add(intersectionsToRender);
        ans.add(rays);
        return ans;
    }

    public static List<Object> getAllRaysOnEllipsoidUI() {
        List<Object> ans = new ArrayList<Object>();

        List<Ray> rays = new ArrayList<Ray>();
        List<Intersection> intersectionsToRender = new ArrayList<Intersection>();

        System.out.println("Введите радиус-вектор центра эллипсоида: ");
        double[] p0 = readDoubleArray(DIM);
        System.out.println("Введите размерности эллипсоида: ");
        double[] abc = readDoubleArray(DIM);
        System.out.println("Введите радиус-вектор начала луча: ");
        double[] r0 = readDoubleArray(DIM);
        System.out.println("Введите вектор направления луча: ");
        double[] e = normalize(readDoubleArray(DIM));
        List<Intersection> intersections = intersectionWithEllipsoid(p0, abc, r0, e);
        Ray start = new Ray(r0, e);
        if (!intersections.isEmpty()) start.setT(intersections.get(0).getT());
        rays.add(start);
        if (!intersections.isEmpty()) {
            if (intersections.size() == 1) { // изнутри
                intersectionsToRender.add(intersections.get(0));
                double[] normVector = calculateNormOnEllipsoid(intersections.get(0).getPoint(), p0, abc);
                rays.add(reflectedRay(normVector,
                        e, intersections.get(0), true));
                rays.add(refractedRay(normVector,
                        e, intersections.get(0), false));
                rays.add(new Ray(intersections.get(0).getPoint(), normVector, Color.BROWN));
                rays.add(new Ray(intersections.get(0).getPoint(), mult(normVector, -1), Color.BROWN));
            } else { //снаружи
                intersectionsToRender.add(intersections.get(0));
                double[] normVector = calculateNormOnEllipsoid(intersections.get(0).getPoint(), p0, abc);
                rays.add(reflectedRay(normVector,
                        e, intersections.get(0), false));
                Ray refractedRay = refractedRay(normVector,
                        e, intersections.get(0), true);
                rays.add(refractedRay);
                rays.add(new Ray(intersections.get(0).getPoint(), normVector, Color.BROWN));
                rays.add(new Ray(intersections.get(0).getPoint(), mult(normVector, -1), Color.BROWN));

                List<Intersection> fromInside = intersectionWithEllipsoid(p0, abc, refractedRay.getPoint(), refractedRay.getE());
                if (fromInside. size() > 1) {
                    refractedRay.setT(fromInside.get(1).getT());
                    intersectionsToRender.add(fromInside.get(1));
                    normVector = calculateNormOnEllipsoid(fromInside.get(1).getPoint(), p0, abc);
                    rays.add(reflectedRay(normVector,
                            refractedRay.getE(), fromInside.get(1), true));
                    rays.add(refractedRay(normVector,
                            refractedRay.getE(), fromInside.get(1), false));

                    rays.add(new Ray(fromInside.get(1).getPoint(), normVector, Color.BROWN));
                    rays.add(new Ray(fromInside.get(1).getPoint(), mult(normVector, -1), Color.BROWN));
                }
            }
        }
        ans.add(p0);
        ans.add(abc);
        ans.add(intersectionsToRender);
        ans.add(rays);
        return ans;
    }


    static float[] createRay2D(Ray ray, double t) {
        float[] ans = new float[4];
        double[] start = ray.getPoint();
        ans[0] = (float) start[0];
        ans[1] = (float) start[1];
        double[] end = sum(start, mult(ray.getE(), t));
        ans[2] = (float) end[0];
        ans[3] = (float) end[1];
        return ans;
    }

    private static double[] calculateNormOnSphere(double[] point, double[] centerOfSphere) {
        double[] subRP = sub(point, centerOfSphere);
        return div(subRP, norm(subRP));
    }

    private static double[] calculateNormOnEllipsoid(double[] point, double[] centerOfSphere, double[] abc) {
        return normalize(new double[]{-2 * (point[0] - centerOfSphere[0]) / abc[0],-2 * (point[1] - centerOfSphere[1]) / abc[1]});
    }


    public static void init(int dimension) {
        DIM = dimension;
        System.out.println("Введите коэффициенты преломления перед и после поверхности n1 и n2");
        n1 = MyReader.readDouble();
        n2 = MyReader.readDouble();
    }

}
