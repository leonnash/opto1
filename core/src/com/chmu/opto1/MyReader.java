package com.chmu.opto1;

import java.util.Scanner;

public class MyReader {
	private static Scanner sc = new Scanner(System.in);

	public static double readDouble() {
		return sc.nextDouble();
	}

	public static int readInt() {
		return sc.nextInt();
	}

	public static String readString() {
		return sc.next();
	}

}
